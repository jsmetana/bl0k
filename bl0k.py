import base64
import io
import json
import sys
import time
import zlib
from random import SystemRandom
import pygame


def _darker(color):
	return int(color[0] * 0.6), int(color[1] * 0.6), int(color[2] * 0.6)


def _lighter(color):
	return min(int(color[0] * 1.8), 255), min(int(color[1] * 1.8), 255), min(int(color[2] * 1.8), 255)


def draw_block(surface, x, y, color, grid=(20, 20), offset=(0, 0), fill=True):
	if fill:
		pygame.draw.rect(surface, color, pygame.Rect(x*grid[0] + offset[0], y*grid[1] + offset[1], grid[0]-1, grid[1]-1))
	pygame.draw.lines(
		surface, _lighter(color), False, (
			(x*grid[0]+grid[0]-2 + offset[0], y*grid[1] + offset[1]),
			(x*grid[0] + offset[0], y*grid[1] + offset[1]),
			(x*grid[0] + offset[0], y*grid[1]+grid[1]-2 + offset[1])
		))
	pygame.draw.lines(
		surface, _darker(color), False, (
			(x*grid[0]+grid[0]-2 + offset[0], y*grid[1] + offset[1]),
			(x*grid[0]+grid[0]-2 + offset[0], y*grid[1]+grid[1]-2 + offset[1]),
			(x*grid[0] + offset[0], y*grid[1]+grid[1]-2 + offset[1])
		))


class Block:
	base_blocks = [
		((0, 1, 0, 0), (0, 1, 0, 0), (0, 1, 0, 0), (0, 1, 0, 0)),  # I
		((0, 0, 0), (1, 1, 1), (1, 0, 0)),  # L
		((0, 0, 0), (1, 1, 1), (0, 1, 0)),  # T
		((0, 0, 0), (1, 1, 1), (0, 0, 1)),  # J
		((0, 0, 0), (1, 1, 0), (0, 1, 1)),  # Z
		((0, 0, 0), (0, 1, 1), (1, 1, 0)),  # S
		((1, 1), (1, 1)),  # O
	]

	base_colors = [
		(0x00, 0xff, 0xff),  # I
		(0xff, 0x88, 0x00),  # L
		(0xff, 0x00, 0xff),  # T
		(0x00, 0x00, 0xff),  # J
		(0x00, 0xff, 0x00),  # Z
		(0xff, 0x00, 0x00),  # S
		(0xff, 0xff, 0x00),  # O
	]

	def __init__(self, block, x, y, color, n):
		self.block = block
		self.size = len(block)
		self.half_size = self.size / 2
		self.x = x
		self.y = y
		self.top = self.y - self.half_size
		self.left = self.x - self.half_size
		self.color = color
		self.n = n

	@staticmethod
	def new(game_width, n=None):
		if n is None:
			n = random.randrange(len(Block.base_blocks))
		block = Block.base_blocks[n]
		size = len(block)
		x = game_width / 2
		if size % 2:
			x -= 0.5

		empty_rows = 0
		for row in reversed(block):
			if sum(row):
				break
			empty_rows += 1

		y = -(size - 2) / 2 + empty_rows
		return Block(block, x, y, Block.base_colors[n], n)

	def move(self, x, y):
		return Block(self.block, self.x + x, self.y + y, self.color, self.n)

	def rotate_cw(self):
		return Block(
			[list(reversed(col)) for col in zip(*self.block)],
			self.x, self.y, self.color, self.n
		)

	def rotate_ccw(self):
		return Block(
			list(reversed(list(zip(*self.block)))),
			self.x, self.y, self.color, self.n
		)

	def pos_ok(self, game):
		for y in range(self.size):
			for x in range(self.size):
				if self.block[y][x] and game[self.top + y, self.left + x]:
					return False
		return True

	def draw(self, fun):
		for y in range(self.size):
			for x in range(self.size):
				fun(self.left + x, self.top + y, self.block[y][x], self.color)

	def draw_by_itself(self, fun, left, top):
		for y in range(self.size):
			for x in range(self.size):
				fun(left + x, top + y, self.block[y][x], self.color)

	def write(self, game):
		for y in range(self.size):
			for x in range(self.size):
				if self.block[y][x]:
					game[self.top + y, self.left + x] = 1


class Game:
	def __init__(self, surface, height=20, width=10):
		self.height = height
		self.width = width
		self._map = [[0 for _ in range(width)] for _ in range(height)]

		self.current_block = Block.new(width)
		self.held_block = None
		self.next_block = Block.new(width)

		self.stats = pygame.Surface(surface.get_size())
		self.bg = pygame.Surface(surface.get_size())
		self.surface = surface
		self.fg = pygame.Surface(surface.get_size())
		surface.set_colorkey((0, 0, 0))
		self.stats.set_colorkey((0, 0, 0))
		self.fg.set_colorkey((0, 0, 0))
		self.stats.fill((0, 0, 0))
		self.fg.fill((0, 0, 0))
		surface.fill(0x000000)
		for i in range(0, 220, 20):
			pygame.draw.line(self.bg, 0x444444, (i, 0), (i, 400))
		for i in range(0, 420, 20):
			pygame.draw.line(self.bg, 0x444444, (0, i), (200, i))

		self.removed = []
		self.removed_cycle = False
		self.removed_timer = 0

		self.held_once = False

		self.score = 0

	def __getitem__(self, item):
		y, x = item
		if x < 0 or x > (self.width - 1) or y > (self.height - 1):
			return 1
		if y < 0:
			return 0
		return self._map[int(y)][int(x)]

	def __setitem__(self, key, value):
		y, x = key
		self._map[int(y)][int(x)] = value

	def draw_block(self, x, y, v, color):
		if v:
			# pygame.draw.rect(self.fg, color, pygame.Rect(x*20, y*20, 19, 19))
			# pygame.draw.lines(self.fg, _lighter(color), False, ((x*20+18, y*20), (x*20, y*20), (x*20, y*20+18)))
			# pygame.draw.lines(self.fg, _darker(color), False, ((x*20+18, y*20), (x*20+18, y*20+18), (x*20, y*20+18)))
			draw_block(self.fg, x, y, color)

	def draw_block_sides(self, x, y, v, color):
		if v:
			pygame.draw.lines(self.fg, _lighter(color), False, ((x*20+18, y*20), (x*20, y*20), (x*20, y*20+18)))
			pygame.draw.lines(self.fg, _darker(color), False, ((x*20+18, y*20), (x*20+18, y*20+18), (x*20, y*20+18)))

	def draw_block_final(self, x, y, v, color):
		if v:
			pygame.draw.rect(self.surface, color, pygame.Rect(x*20, y*20, 19, 19))
			pygame.draw.lines(self.surface, _lighter(color), False, ((x*20+18, y*20), (x*20, y*20), (x*20, y*20+18)))
			pygame.draw.lines(self.surface, _darker(color), False, ((x*20+18, y*20), (x*20+18, y*20+18), (x*20, y*20+18)))

	def draw_stats_block(self, x, y, v, color):
		if v:
			pygame.draw.rect(self.stats, color, pygame.Rect(x*20, y*20, 19, 19))
			pygame.draw.lines(self.stats, _lighter(color), False, ((x*20+18, y*20), (x*20, y*20), (x*20, y*20+18)))
			pygame.draw.lines(self.stats, _darker(color), False, ((x*20+18, y*20), (x*20+18, y*20+18), (x*20, y*20+18)))

	def move_left(self):
		if self.current_block:
			new = self.current_block.move(-1, 0)
			if new.pos_ok(self):
				self.current_block = new
				self.update()

	def move_right(self):
		if self.current_block:
			new = self.current_block.move(1, 0)
			if new.pos_ok(self):
				self.current_block = new
				self.update()

	def rotate_cw(self):
		if self.current_block:
			new = self.current_block.rotate_cw()
			if new.pos_ok(self):
				self.current_block = new
				self.update()
			else:
				for x in (-2, -1, 1, 2):
					new_moved = new.move(x, 0)
					if new_moved.pos_ok(self):
						self.current_block = new_moved
						self.update()
						return

	def rotate_ccw(self):
		if self.current_block:
			new = self.current_block.rotate_ccw()
			if new.pos_ok(self):
				self.current_block = new
				self.update()

	def hold(self):
		if self.held_once:
			return
		self.held_once = True

		if not self.held_block:
			self.held_block = self.next_block
			self.next_block = Block.new(self.width)
		self.held_block, self.current_block = self.current_block, self.held_block
		self.held_block = self.held_block.new(self.width, self.held_block.n)
		self.update()

	def update(self):
		self.fg.fill(0x000000)
		bottom = self.current_block
		while True:
			new_bottom = bottom.move(0, 1)
			if new_bottom.pos_ok(self):
				bottom = new_bottom
			else:
				bottom.draw(self.draw_block_sides)
				break
		self.current_block.draw(self.draw_block)
		self.stats.fill(0x000000)

		if self.held_block:
			self.held_block.draw_by_itself(self.draw_stats_block, 0, 2)
		self.next_block.draw_by_itself(self.draw_stats_block, 0, 8)

	def check_rows(self):
		self.removed_timer = time.monotonic_ns()
		for y, row in enumerate(self._map):
			if sum(row) == self.width:
				rect = pygame.Rect(0, 0, self.surface.get_width(), y*20)
				tmp = pygame.Surface(rect.size)
				tmp.blit(self.surface, (0, 0), rect)
				self.surface.blit(tmp, (0, 20))
				self.removed.append(y)
		for y in reversed(self.removed):
			self._map.pop(y)
		for _ in self.removed:
			self._map.insert(0, [0 for _ in range(self.width)])
		self.score += len(self.removed) ** 2 * 20

	def block_done(self):
		self.current_block.write(self)
		self.current_block.draw(self.draw_block_final)
		self.check_rows()
		self.current_block = self.next_block
		self.next_block = Block.new(self.width)
		if not self.next_block.pos_ok(self):
			return True
		self.update()
		self.held_once = False
		return False

	def fall(self):
		self.score += 30
		bottom = self.current_block
		while True:
			new_bottom = bottom.move(0, 1)
			if new_bottom.pos_ok(self):
				bottom = new_bottom
			else:
				self.current_block = bottom
				return self.block_done()

	def tick(self, timer=None):
		self.score += 1
		if self.removed:
			return False
		new = self.current_block.move(0, 1)
		if new.pos_ok(self):
			self.current_block = new
			self.update()
		else:
			return self.block_done()

	def render(self, screen, game=(0, 0), stats=(220, 0)):
		screen.blits((
			(self.bg, game),
			(self.surface, game),
			(self.fg, game),
			(self.stats, stats),
			(font.render('H0LD!', False, (255, 240, 230)), (stats[0], stats[1] + 16)),
			(font.render('NEXT!', False, (255, 240, 230)), (stats[0], stats[1] + 136)),
			(font.render('SCORE:', False, (255, 240, 230)), (stats[0], stats[1] + 280)),
			(font.render(f'{self.score:>7,}', False, (255, 240, 230)), (stats[0], stats[1] + 320))
		))
		if self.removed:
			self.removed_cycle = not self.removed_cycle
			for y in self.removed:
				pygame.draw.rect(screen, 0xffffff * self.removed_cycle, pygame.Rect(game[0], game[1] + y*20, 220, 19))
			if time.monotonic_ns() - self.removed_timer > 240000000:
				self.removed = []


class Main:
	def __init__(self):
		self.tick_ev = pygame.event.custom_type()
		data = {}
		try:
			with open('bl0k.json') as fp:
				data = json.load(fp)
		except (IOError, json.JSONDecodeError):
			pass
		self.music = data.get('music', True)
		self.video = data.get('video', 0)
		self.scoreboard = data.get('scoreboard', [])
		self.save()

		self.screen = None
		self.init_screen()
		self.init_music()

		self.clock = pygame.time.Clock()
		self.key_clock = time.monotonic_ns()

	def init_screen(self):
		size = width, height = 640, 480
		flags = {
			0: pygame.SCALED,
			1: pygame.SCALED | pygame.FULLSCREEN,
			2: pygame.FULLSCREEN,
			3: 0,
		}
		self.screen = pygame.display.set_mode(size, flags=flags[self.video], vsync=True)

	def init_music(self):
		if self.music:
			pygame.mixer.music.load('bl0k.ogg')
			pygame.mixer.music.play(loops=-1)
		else:
			pygame.mixer.music.stop()
			pygame.mixer.music.unload()

	def run(self):
		while True:
			self.main_menu()

	def show_scoreboard(self):
		self.screen.fill(0x000000)
		self.screen.blit(font_large.render('SCOREBOARD !', False, (0, 240, 0)), (250, 20))
		for y, i in enumerate(self.scoreboard):
			color = (0xff, 0xff, 0xff)
			if y == 0:
				color = (0xff, 0xd7, 0x00)
			elif y == 1:
				color = (0xc0, 0xc0, 0xc0)
			elif y == 2:
				color = (0xcd, 0x7f, 0x32)
			self.screen.blit(font_large.render(f'#{y+1:0>2}: {i:>7,}', False, color), (250, y*40+80))
		while True:
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					sys.exit()
				elif event.type == pygame.KEYDOWN:
					return
			pygame.display.flip()
			self.clock.tick_busy_loop(60)

	def main_menu(self):
		# self.screen.blit(font_large.render('BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!', False, (255, 240, 230)), (0, 0))
		# self.screen.blit(font_large.render('BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!', False, (255, 240, 230)), (0, 30))
		option = 0
		options = [
			self.classic,
			self.show_scoreboard,
			self.settings,
			sys.exit
		]
		self.screen.fill(0x000000)
		block_map = (
			(1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1),
			(1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1),
			(1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1),
			(1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0),
			(1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1),
		)
		color = (
			(0xff, 0x00, 0x00),
			(0xff, 0x88, 0x00),
			(0xff, 0xff, 0x00),
			None,
			(0x88, 0xff, 0x00),
			(0x00, 0xff, 0x00),
			(0x00, 0xff, 0x88),
			None,
			(0x00, 0xff, 0xff),
			(0x00, 0x88, 0xff),
			(0x00, 0x00, 0xff),
			None,
			(0x88, 0x00, 0xff),
			(0xff, 0x00, 0xff),
			(0xff, 0x00, 0x88),
			None,
			(0xff, 0xff, 0xff),
		)
		for y, row in enumerate(block_map):
			for x, column in enumerate(row):
				if column:
					draw_block(self.screen, x, y, color[x], offset=(177, 40))
		self.screen.blit(font_large.render('PLAY!', False, (255, 240, 230)), (250, 200))
		self.screen.blit(font_large.render('>>              <<', False, (60, 255, 60)), (200, 200))
		self.screen.blit(font_large.render('SCOREBOARD!', False, (255, 240, 230)), (250, 280))
		self.screen.blit(font_large.render('SETTINGS!', False, (255, 240, 230)), (250, 360))
		self.screen.blit(font_large.render('EXIT!', False, (255, 240, 230)), (250, 440))
		while True:
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					sys.exit()
				elif event.type == pygame.KEYDOWN:
					if event.key in {pygame.K_UP, pygame.K_w}:
						prev_option = option
						option = (option - 1) % len(options)
						self.screen.blit(font_large.render('>>              <<', False, (0, 0, 0)), (200, 200+prev_option*80))
						self.screen.blit(font_large.render('>>              <<', False, (60, 255, 60)), (200, 200+option*80))
					elif event.key in {pygame.K_DOWN, pygame.K_s}:
						prev_option = option
						option = (option + 1) % len(options)
						self.screen.blit(font_large.render('>>              <<', False, (0, 0, 0)), (200, 200+prev_option*80))
						self.screen.blit(font_large.render('>>              <<', False, (60, 255, 60)), (200, 200+option*80))
					elif event.key in {pygame.K_RETURN, pygame.K_RIGHT, pygame.K_d, pygame.K_SPACE}:
						options[option]()
						return
			pygame.display.flip()
			self.clock.tick_busy_loop(60)

	def save(self):
		try:
			with open('bl0k.json', 'w') as fp:
				json.dump({
					'video': self.video,
					'music': self.music,
					'scoreboard': self.scoreboard
				}, fp)
		except IOError:
			pass

	def next_video(self):
		self.video = (self.video + 1) % 4
		self.init_screen()
		self.save()

	def next_music(self):
		self.music = not self.music
		self.init_music()
		self.save()

	def settings(self):
		# self.screen.blit(font_large.render('BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!', False, (255, 240, 230)), (0, 0))
		# self.screen.blit(font_large.render('BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!BL0K!', False, (255, 240, 230)), (0, 30))
		option = 0
		options = [
			self.next_video,
			self.next_music,
			lambda: None
		]

		while True:
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					sys.exit()
				elif event.type == pygame.KEYDOWN:
					if event.key in {pygame.K_UP, pygame.K_w}:
						prev_option = option
						option = (option - 1) % len(options)
					elif event.key in {pygame.K_DOWN, pygame.K_s}:
						prev_option = option
						option = (option + 1) % len(options)
					elif event.key in {pygame.K_RETURN, pygame.K_RIGHT, pygame.K_s, pygame.K_SPACE}:
						if option == 2:
							return
						options[option]()
			self.screen.fill(0x000000)
			self.screen.blit(font_large.render('>>               <<', False, (60, 255, 60)), (200, 200+option*80))
			self.screen.blit(font_large.render(f'VIDEO:  {self.video}', False, (255, 240, 230)), (250, 200))
			self.screen.blit(font_large.render(f'MUSIC:  {self.music}', False, (255, 240, 230)), (250, 280))
			self.screen.blit(font_large.render('BACK!', False, (255, 240, 230)), (250, 360))
			pygame.display.flip()
			self.clock.tick_busy_loop(60)

	def pause(self):
		option = 0
		options = [
			False,
			True,
		]

		while True:
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					sys.exit()
				elif event.type == pygame.KEYDOWN:
					if event.key in {pygame.K_UP, pygame.K_w}:
						option = (option - 1) % len(options)
					elif event.key in {pygame.K_DOWN, pygame.K_s}:
						option = (option + 1) % len(options)
					elif event.key in {pygame.K_RETURN, pygame.K_RIGHT, pygame.K_s, pygame.K_SPACE}:
						return options[option]
			self.screen.fill(0x000000)
			self.screen.blit(font_large.render('>>               <<', False, (60, 255, 60)), (200, 200+option*80))
			self.screen.blit(font_large.render('CONTINUE!', False, (255, 240, 230)), (250, 200))
			self.screen.blit(font_large.render('MAIN MENU!', False, (255, 240, 230)), (250, 280))
			pygame.display.flip()
			self.clock.tick_busy_loop(60)

	def classic(self):
		while True:
			game = Game(pygame.Surface((640, 480)))
			pygame.time.set_timer(self.tick_ev, 500)
			flag = True
			while flag:
				ns = time.monotonic_ns()
				if self.key_clock == -1 or ns - self.key_clock >= 100000000:
					pressed_keys = pygame.key.get_pressed()
					if pressed_keys[pygame.K_LEFT]:
						game.move_left()
					elif pressed_keys[pygame.K_RIGHT]:
						game.move_right()
					elif pressed_keys[pygame.K_DOWN]:
						if game.tick():
							flag = False
					self.key_clock = ns

				game.render(self.screen, (10, 10))
				pygame.display.flip()
				self.clock.tick_busy_loop(60)
				for event in pygame.event.get():
					if event.type == pygame.QUIT:
						sys.exit()
					elif event.type == pygame.KEYDOWN:
						if event.key in {pygame.K_LEFT, pygame.K_RIGHT, pygame.K_DOWN}:
							self.key_clock = -1
						elif event.key == pygame.K_UP:
							game.rotate_cw()
						elif event.key == pygame.K_SPACE:
							if game.fall():
								flag = False
						elif event.key == pygame.K_c:
							game.hold()
						elif event.key in {pygame.K_q, pygame.K_ESCAPE}:
							if self.pause():
								flag = False
					elif event.type == self.tick_ev:
						if game.tick(self.tick_ev):
							flag = False
			option = 0
			options = [
				False,
				True
			]
			self.screen.fill(0x000000)
			self.screen.blit(font_large.render('YOU DIED!', False, (64, 0, 0)), (245, 100))
			self.screen.blit(font_large.render('YOU DIED!', False, (100, 0, 0)), (250, 100))
			self.screen.blit(font_large.render('YOU DIED!', False, (255, 0, 0)), (255, 100))
			self.screen.blit(font_large.render(f'SCORE: {game.score:>7,}!', False, (255, 255, 0)), (255, 180))
			self.scoreboard.append(game.score)
			self.scoreboard.sort(reverse=True)
			self.scoreboard = self.scoreboard[:10]
			self.save()
			self.screen.blit(font_large.render('RETRY!', False, (255, 240, 230)), (250, 280))
			self.screen.blit(font_large.render('>>           <<', False, (60, 255, 60)), (200, 280))
			self.screen.blit(font_large.render('BACK!', False, (255, 240, 230)), (250, 360))
			flag = True
			while flag:
				for event in pygame.event.get():
					if event.type == pygame.QUIT:
						sys.exit()
					elif event.type == pygame.KEYDOWN:
						if event.key in {pygame.K_UP, pygame.K_w}:
							prev_option = option
							option = (option - 1) % len(options)
							self.screen.blit(font_large.render('>>           <<', False, (0, 0, 0)), (200, 280+prev_option*80))
							self.screen.blit(font_large.render('>>           <<', False, (60, 255, 60)), (200, 280+option*80))
						elif event.key in {pygame.K_DOWN, pygame.K_s}:
							prev_option = option
							option = (option + 1) % len(options)
							self.screen.blit(font_large.render('>>           <<', False, (0, 0, 0)), (200, 280+prev_option*80))
							self.screen.blit(font_large.render('>>           <<', False, (60, 255, 60)), (200, 280+option*80))
						elif event.key in {pygame.K_RETURN, pygame.K_RIGHT, pygame.K_s, pygame.K_SPACE}:
							flag = False
				pygame.display.flip()
				self.clock.tick_busy_loop(60)
			if options[option]:
				break


# TODO: speed
# TODO: delay after placing
# TODO: 2 player mode
# TODO: wide/special mode
# TODO: joystick support

# FIXME: when on top some on bottom get away

random = SystemRandom()

pygame.init()

font_str = 'c-q~43wRvGnc!d3BgwMHmfQ09ZE42ik$K1uX*9?f6EGMs#F%FsC4qpf9$Nxg5|V5TaeyUC!e-g5Hsrx>Se6R}oP@hItV3Ahu!JQ^Z#I`ph?1}t0pA|C$K|+PE~g}$#Fp-VR8?1xBp^V(%iV}^SNBx?_19njyQ&&c05BJB0}rk}|NJX2{^)0Kc^+U_D?VL*!ItyRhcqm}`^9vB$rbBb23Ob30#IFee{JWUj{b(-cg+S!Vx8m{JNFG`py>S^VAfOkdvbTjVE^PKLWW)Q@P5|ro||^P<G-qB0csG&CCRQ*M^L-<>znbnSMYmU7e1(ZR(%TLufqFPU3-T1=a(=22HuANiHmyrIy-*&%1<{TsAuqg%bt$?{c5LLkH2rl`kCI2J*Cfo`m*l=yf+0<ab|zt;LxeJ4j%@%W*b1|(?Daz@AtRX|MzQZH~k!HD$m9z@NCQ66?8rN<DPE;O!Rx1<RQWr5+EOJlRPxh58!3!7XMWc|0L;&%Hlr^(YB<|TB+e>)Cg1{9%oapUdCJ@NVIc}YTLqdLGM_5`C6PGXuMu<NFG8s__>X`IpV!cBM0RX<uihzU}--{I|#TG*k^$)2TnO^;noZ_1pHSumBBFtYT`CiAw?KW{RFX6XzPX3v=3&0;=n*0f>`}v>bRXculI>lAU?8S#`<o((4n#U2AYCp^B_-E6J5<Wg2U7e9ZFAAp812^5=_DR^+x0A?{eXE?*Y71e4q60!0Wxpew9!Ux5EAKd3XpOhDYHm@HO~b_!j&<d>?)U&%-~#zrr~D5{|>9`h<6vcei)H_n`L$?-B3I-c#N)-ZwKVGn+EEW**9ZDtmC{(v=M>PhPooWyi`=E|Hs;%jO}U$fxr2@(c2d^UL$A@=f{H{HA<Q{<i#Z{*L^e`MdJ><sZ&JntvjHB>znQAM*d$xT*2%#&a64ZR~8^)p$K>9{SwJ_<a!$!I$9)#P1o4-;3}P{4e+g{Avn*hY`QObMPBM{Fbb&*Z2jDUuFjUF17fLl;QVW<3@{Lsqs3(Z}LA+9-Dk|@+Xr=C!d@A(c}*&zd!lC$-kfc#^h6zPfmVq^2p>@C!d&ne6sOG<B63g7N1ynV!??yCu&Zp<CDiHj@^IkW5>20%f5K}i>qJw$_s~I_|gl1{KDQB27mJ7pM3i#Umg3&*z;pQ82jGXe;oVv*tf=>8vC2EN5{T2cK6ta$8I0Hb?m^{O=Ek;c8*;=wqxvFW825Jj%_;n{Lz0r`rOg49{ro6k01Rq&i1|V`}|SJkxA62l~9G6x*BFf4WwWW+Oh=H!aVR{KGdP@O2a}}1dCw_EQMvrzblYy8&G3rVI|~X73852R>MhfGMoaZLKCb(eZ3CWLo>8M0a~F5r$HNRfYV_koB^BQOn3*Jg<5_yoCD{=7B~;ihYMgUTnHDzJ7F7K4BO!nxD+mf%i#*R65a(n;N8#;?}7Ki``{|L8r~1rz_ri;JE0S0uLQebH*`TaTnE=f5A1<n=!1T^0R~_YhF~x3gBxK#+ypnn2jBpF5N<)KL*=h3@#`fkDywEy&#p<$nVYDc=g+TOkY2cG@sg#>manL9$YfXMR^=O4pLFsmr#7uwyKa4ROQE%RTHA)xH=eQS%y*o1_U3cWMH_#?)(bCs=eCQtUvlYXmtS$^yLP<0{XOq}-&I$?|C(z%c6J7(UAw!wue-iyPj6rU4FiKid-vVA|E8NiaNvWt+<M!mK7IF}{rP9_`OACn`*-lU2Oj+VLkGY3g)cq)@BivQ9C`!}|Miz2gO7b^_#@x=_+QABeFmQWqaQ!_;XlUW*WsfhU%MYZdHWxJ|M4Hf9e)ac;7l#468-s;*X0BDH*nsXkp8!!9{Mi)Of6I!)pm8A`nY;R{c~b&Vtt|~acAP;#IuQ4y?NeMUYGX??_uwo-ajQFIWO6o+?EXR@3!RK$!C)PR&i3rwG{&uBNY!<e6QlAieFSFD(6<VR9;m%P<dZvSovzzysFl!E30m+da&y8s_$0)de+=o8)v<L)-AIhob{bqzo=eZ{f_Eut4FF2RzF$&+-#V=boPeXJ7(WL`*X9uIs2zIi)&hIE~@FN`B=?kH9txvQwvj@QrlC#shd-GrVgc^O}#p2!JG|qu9`DE=Ycs-&Ut=rV(!AZEpxZey>9NEa}Uk^?%d<G%WF5(US8W@dr$3=+Lz`f=PjJqI&b^D0sQ;;yu<UJ_bdI&{Q>_0|FHjE|K<6q`Hl0>pMT~2ee(~_KUTN0?)<u&>mIFpZb53nh6P;<?pyHmf>+Y@=}XfeNk5kUmxW6gp0jXh;R6egE}FflY0>sYLyJDS=;=kTEMB;H%i`X}A6a~8@zEu-maJcL)sownd~(S%OI}^NeCgJu{Y(FR>61%Gm%h3zwQS?E?aO+W-LdSkWuwdfW%=yotCnwF-n;zn<qs}@dihJs$5+f=aoUPYR~%UJ;EHdrIJ)B1`pWwG^_lwh^=H>#ULVx&ufMbY-ug%Dzf*s#{$~xd8tNN1G;C|Qwqbw6#~U7Kc(P%%;is8oW_hMHvm?`+xg+yn=E=-xW;{DL+n7B++m*c|dpP@a_IueEvae)+z4EbDnN{Ceb#&D~ubRl$=TFP;$lsj*bpGl5zcsEx9ehXQ;l}40kFTy@ea`A@SKp3WqlohKLh=;UD^;-e5e4fuJzAB7|Jm|LWySNG9`zDfghxDjTtSZ?t*TVVH$AH8dBM;6`K+HUsxM63sm_}Cdh(Rt{HW+XWjsMIgML~K>Ln-^ic~r4=h9h~E))aw{wDf^iBUBYhC$-U#OR5Gfj1nmQLeIWJ#Jlxnrs<rFDUv2zpj`~=hC^NS1hEnxniL%=ND8qo%Qi~5U7zL#DALtx}4aOXg_f<(H?l2<HzWk{DRNvfpnL2@RWY27lc(HREu@#0%BVz<_pDKMKPB~w6Xm`0*~lZcL+QP5E_@cTOI`a59p_65GE{92qVp3gvx(K`or2>pAnPx1XY7z7y-jTtb8J2kzw#(437H|IMFAXi(#at!A9WFg!rDmBV@vf(U1l~J{c8of|@2qCq@IDCG8UtoEh-J8NnxvPbkE|;e<HFf(eh7;h_JPXh;9Q1|UxZ5HLA4&J}b;!dpir9`Vo;mGzZO2ewJ)ybMl6D7{pk{i~-EN4PPKHz2wtjv#y-Z(B5<XS(f$RD>b-sBiQnYH!f#f0@RB#^~pW3;jBcH%$X!V?nrBwVQr4SL*rT7{+k5rX@o48MSml^WPcD&&6hy&<)MqM1QDRhGQcdx4>mSCdtqP&lxPKlQw)3YKS3y=%U6W<ikJ!BA%1?uxyYF0!uJL@%5D`fn=tUB7ZQ)pj;hbRz9&M@Lr~|R$89%v=ryi?*N*J)vOfoPmn`S%oXujTH%+Zy8@?*unPnI59x|<A{7dg5M$ElkwfX$h@&GrNQ>k);(%t~aH%6X_$QOi+?m`K6Pb8>%j=A>$Q=EJj?|6I^*4tFf)XC&IfgbOyq|OA1&OHTmq*78qral4Xkcb6NjtBkqgkJ}VRMvYf04e*MAAn!C5|BV_0`eCbI>=0ER&grK)itsk#7Y9tc_~T8$Nyjhk`f|zk=ogrb7Y!KWHYcYsaresmN@W&hfvlHlQmS2P1H#V`Q=n$wJ~BeLImdBM+=1ExVD%p))_orXJ02)~E7BKhs@k)$m$$7fltX{fUE9$*+0Az(mT|#h~SUE$ty`pMZ@E>YcJSZJN)G#tua20<wHKS1!+nthefj=ZMY@$6~Ky-B>?6O1`|E;D?k?%dC(-rWUN}Vhx=>+bKsw_BD%hG>)Q%<B#JM>HKsdhL;}G6dNRh#TVC>)w1-*Y=@m@k%17(>ic5zt-Uj}PP7~Kg?vi6vUcNd)cM-l`XnCdoLCf`rFNQMvS_G2`a{;4G&<z(3bICZEZ&NBCMy}TUCC`oI#xH4?j9E1eWud<rD>x1i)oTK+@a@8c%S|uq}isDDo407-pLJ!$04H~kwek3%J~krv3)QKU1(0DFq9@|O6V=xoqJ-{II7Kt$+2mlf^;#ss;%fMM^ee8jSJ~q+@Zj6gkcyHOqvKLr(@gl7+Y%FW;Fy!UQtu2sMQc_2+_qNdm@tJdk8D(C!*t<_(DAje+DG^IDhChDi#y;2T4vsa?DtVDa#vm;tlA5`i82Z^;beN*W%$B`Ec4s`oEAizFZh1gJ~>s<7Fbb$!Q)DQ(VwIB05McTo_K{yOACVwJ%fdE9sB+&BDoh(1<q?9kN0OpSV|BN-fK31gSMnTtG5Klp|a@_co3r#pii!E3h3OcG+w5Ng02{E)LI$k6q?r7qY%lZ}gXCOFEHq^|0$<vtt<3uXA`_jF!haHI|fT1N{*@j+cqwoBe1QOef2k@`o6ub;qH#>d5hyHzQtg{0y=4B)25BB&8ZM4o=1y(k#%|K*cc{F%68229ErsV_r6oOr)jYuPzid)kEtN2HMR=uw=Se*TnFvv-tTGE@d6OQW$`x*#8rp5W%VVP;{i_S+ub;gIOj~ph2*z6di5Cq&ObFo^xNTHzF6;4~6dD%hU=zkP#jT#CXVw(jo^p!lcO9L^L*JH>B1B<GYx?sxz^X(Xa@LP?Pa7Joe$WEf7m0)*DL%EJ1m8HIDK^ei}(Dv!zHAt4$C)#dWG!qJ{C(TyI2_fVksf#N)%nw{=rQ&-z+1e>9AVp<&>dQb|xgo^=Z2NfEW8=vks=R=Q0__t^g1Y>c9Pzp4JT^@qc0ow#^B8qwU3WKc6iHeE+nhmjZiMgElpRqXCl6cF+DFh?hmPN5R<cu{o8C|QybWCda|;)s!urY(-gGDkC{wRZQj81R``QN|-SDTtgsa;rFOjq#}DEXZ5tNYQX;p80_yI<XXp8cjIqw=%YL<fS2!Iz887xr~=5e!xg%I7uHc{LomkQ1o!Z!Oz7`^ZROT+zi*q<Pmb(9wN}VzS9sSBJy=}Ez&|Ht1l4@sU;ewj1)~$1Z&ZZu+k2ksZiW8ErIL|lwulH<OIA!D^d#M9Qc>fOT3I!d>(3;Lr`@wXZY*|O^lMNQX|I~t8r65@<SnCh>``QQ*DS!M0H`;itlam3apnylb#@LV@%-m^p`l=<_(x~HVYQfUpui{h-gu@42Nc$*)VgcBM#HO^&=u(u48NQjC}S>{m^*-lmoz)izRC|Q`<CSuV+5qRy84MM|5LXUSm>W^mVRy1|=5@zRB-2xshBD_wGapp-e97;!=yWO?y^i2~td6Nn6?&*~nF-i3)5(&QKhR7#lJw_@L|)W1rX(N+8%iMK6|V4;mDO`bK+%4lW_vO0?sQPt4yDn;%8&Ws$n^0eG67h#d7~C_Yc9<1!w7SuE^ltq)I-1Tl8ck1(r|p2bm?%TdwDk@Ou>Z<KkUZ;Z#qB8#^3<GIt&O$$gEZG-8cjL$^2<j+KBp$^TEgu|%D$)X>@-ff=K&@W1Iq$8!9iwtkt@x-jO8PD7)#C+B|j!+FghJHmuXpV2#vQW^j)dsw#ADhk@r^<M-EWdi%w7oK>-OZngfpzp&mFO+<$<R^~4j~JpYi~Ih>gLqktXNqtKs<`FluB8Dti>=690_^E$kfLd^E^-lg)#A>Y4XjQ_>QN+rYaUYVNC61ChsB3oJJ~9u8W_C-mn@+xD+!AC=)Zj9K`&3^pT82(_gw^B3GU>aV|M33E7%78OL6T8`CXwbXLc=a_8%8Lz4&`&Am4)2Ckpv;TX@li%aC^q=jPQ0cgS1lH`OJE-x>MGR<r=WE`6N0-c!%v{~dakuYkrWyXOx_!ya3EFv80vJ$&d8ZWd_0O1nKyvtw&pLW4VG+e=VEVhnvqHI7AMM*Z55#>uHzD{#&oz?LGC8#CKAa+#3A)N~8x9KwpFT@jc=c&fA0gO$Z&LnA!SY@y^C4EcQM0B^|=jtdTrS_72VuxZh<k*i^x?YxCNkC&Tx!E=mBVHq;928}sLY?p6Wim1tWQCL4!WsR5a`iNwACS`VYiR9xguCOj7>}h%nz?-FbIvtwb8P&_{1Y1Q_{2eUiAaH^9NwvtY@W2zR?-i}(u9VXhNMor8tow!V=<@A6lg|c!5epZV-fR)&&u0(tUBi$b-x^5NNmt=7lh(4ie-Ey*<+S}%T_qMajpu>WH)X3J#G12^SYHi<9Tz~?6fs@yf~vBPtR<}rwhRx{z+TiDzayy3P04jL<;8H<tosK#$|KL7M8pn$#b*Rksb&+s0{SKkqyLlwVaFdtzCEX;^a-!H)hMN{(B8uo@giigME+d6kd#m&GG!*lsurZCox;@Dx%1OMf_36wxy``kM?VFY4XSVIq0{cIE~5Zn+UHJPHrv7QyNc3WrZWG2@lC-bc5@%d{`al3N**&(lRfo2g1NC=?1dG6UCe$xeLlm$Ql!^6LES)au-y8q^u9-7tY~ot4RJ|7)X}eCZMgt*0x;yMji1RPD=~>>r7wdBP5(PfJM$asrESXS>KJ#&H9nEQYvYT^0fhn>x#a!Ho%MC;+}iMj=z@Nrq}S|xp?2^VvH|pTt1dRD|}%XUytPZp5E*UL0pzYFeeU%ar>7FVt#l7&3BYtw)tg_7$o-|2Cg&8ycC)gzRqX_zsGBR9%a=SFHbBbXRHcrUJ*fzuX3VbI0j<hc3eD{QI?K`vp%-kT_KC?WPYmUELt64#t}~4T;ckfypqK0XyoPP{KSz^{2{$IPru?AWPQq{ybS(<wL~2Fn*9&FcAJs?56zsXi?$qi8Po(akHT=F<iylC`nKEL>~XFv&t1?t?$Aa9oWgSEfc4uwX8kQ)j5IE(r$8@Ykzb${br`@~UTan)fV2^T{B<Sk5WGNPd7zznpjLF8@IZ`;j7mgF%?4)Cpo|Y>6*3oFWv!dC=2|GaDSu~u%5L0=*;P-k<89;hwNst7&S@4-Jio9+Ir|ExuA_uu*>Va^?nFwA8)}BK{tB<#A9=IIT(la;o=jQ4vQ$Z?A{D$w_jb(Ft`Uf3wb22uB8IX|z_|szJjqV6*0)KkBMDAJEXs{?vb0|*?d!a(vpPvzmjrL}$|TdnZYZVChB;+S_=izHhuE(0ew1c6m*nJq{YY@KZM;g!ir6Sk&1<1u`ICS!rYPd{SrYYSVx6ond7ZS)ylSFk?79#Uo#SZZcOa~j%}%cA^TGwaf?;%-n;~WeJR;k?#?7)zDq6CS;yt7h=h|#$u*mxg*i@;Azi2<-k&u_Th|tmxJB_l?EqOqP77RBxeFo4g^A2oAFpT_!VaW|1VEU&Fc(b3+@(|DMba_JNp+b}^6h4abh1Lj(_hTJt8YQ`KtXW1voAcLty)K^HqF(akVHmFGc!APXlBz;|Nb9UxhEdm2Y>ga}Hm-rr6XY<uMvgZt8fGD!xlmjt$%yM+drmyd`<HSvXE5q=kwa6S%?Qjst9Vj_Y46JY>-ORD$+Ue;=^_(M*T?GD>!V=T)u;5M)%Q$2m95`I`%S0q&BL2KuTaH%ku7S|P)zo;crkjt=4h;0D`t@irpq&#bkhve6OWs6Q>>j_b&i^7R!EEe(Gi#s%~+o>6g4NCdC>RNzQGFP7iVKWain`h6NV<QDLY{7Mppsu(1rLvJ5zSakMU}Grb#nvEZ^kp2YiElXzMAv1E=(1f@`M>v&$h4KR@Fbrts;MJ~X4o2h!xmMM=x2qAylV*BkLD^=MX7Z7-qG)3HvFM>+2o+I^(rtES0Ujhx~-o7akL2_mO=Paa>a^IJCNLKxKq!^wv^eo1Jz^Jv0SYRr{=6Mt2cRxE?zP|?CMft)}Zj>&ekhf-n)qK}2>>1>;}?`W{X<Hy`&U8EbT*f+6Rtkq@KszR^T=$$uWuUH05X3KFNIo(xeL#FPJnZo(gFumlyB8Go`m@%zf4$}eI6ig4ZVJ4Vy?w*dZ<B%D4(#gNpMt-D$F5K*AmDv{%F*4g!0~Oi$#1^7MsOMbr2cF(-fpp9x%+-2V#za5wAk?0=GrlUjPicyktaCD4X0v*f=h8fP{Fq62%?#6_znTKm&7B#Eus~gyi_E!*g7_P;8HnR*QxrBScf|7H7K3ln;qw|h57z1#OsO21JfVT1Kxm+mSMOPWHQKjhb7WbaH#7EBvSntM4ex>p!zgbiE9Z89D6Q>CoSPO|bW<w6&HtNNBQP;5ikvk;cCVfpGw&rPoq(PmukMeA#}7EZ!7$Ha?zL8*i`wm;vI4E6hL}UCZLG7lmbLjnaow{{vPEYFB}c^GsL4nYg+gaPdzlV3`&CUAOVW%5dpH2+fw!`TvPv#-E^Uam?o5>Y)O#wO{V5zB#3CrNMB26XWgRlU7OzIkJx4csQ*O%5E4ycL;(1zgE+WdyXhS*f;z?O*_e=!a$u={5iR-e_BDg*kA!i@-61{uY!g^UFI``Ht>(gzq9dTK^-z3mm;#j59S3<<l+1DTKuSDIH)yWcy3)!zg>6A!;#B=(!kX&>|gyt-wf>{Z1`|#s=SMDus(`-?^EbH~2J-0#gkd@pydwM3m)VUx^0x#mb$RV=FNX)KD#!UOh;Tt~x^A^uN=zLLZ&8bZ<Sm|B2oNFWXBLBv09IfLu=yfid+a)C2OH_24Kz1?fEoLSOfu2xE2h3g$Ly1oHj8C(_|F_(y|7Ol0yxH>vZ{_U5TRkW6_MMygoty>HIm&T2M>+1~_}M1a>Xm-lL?NlFBJB|!k+tZNfHG3#Ez8=CXZMQh9741URivAD{g{)7dKZU|0d*SS4V~i<J0Sbm^xA9I*Qt5g;YH^n-8^ZnKI0JcaJ`vUZ$hUuB|ECe8S}@^RI>9_@m_SviZBmHdr-^lS6!|wvFXGn9zqGrHlQck%XYx>W`^nPqHA*FJ6kIZTnm|Y|AteNwQt*cRctMx$h);^gSPC&5FVs+iL#eU>9he&3Fp@d3OWYAex+i8@hf<*OHH(Ps{JC#Axw-Wx6!FJeLld;MCSwO-7RuHfHt}EIY>~n(mx(15B=&XdXbC91>01RJn(ta46TKa6FUtPOlm8?%eshU^^YIm_qC8bjqn*uYR_XOcVHrwJVa**Gxpp9_b+vdRENlyJcL0St2%>+-qXWyasp{IWKhSC@mp^+KBityL*tAtjS1zz=uSPZDxpO*vJsIKOE0I-qF_&_jk(hpESUp?o2e_?lbbY-rhB$uBdilVgF&8WgzD&n5K@n#m?Jzlyqn2CH6hEksL1prY1$yHYBNV<bM!pXVmbCqA-wDb(c`Yx^kEBws*BROYJ?_L4`2FbxD~>Q1hyio&wC-T3TFqH8!Y_=7hkepymy4qVA=-1IV^7)qHzfx!Z$ftQwt3DFct}^XK;|3s|Dr=?;6*&LxTIV!*A7kF4C6gMuqR#F_-xbow^0>)Dm%N=_Sw{iq3}6!t!yAsa?emA=T#`05)=<yL?@(`MMbKwlD>4_c41~=<JTgB`anbVbdaDpe4cJV(J=P5Nuvf1dVfiOq<OtaNb)R$xn?-ms~`dQG+~7lAe3!eKX#i;XMh8U9m&%H1#XQlT*wmc_t}EmB{&$mqOgM@{sKuzd20Ds39i1HCA?uj{G_@%tX_?!LlJZ=9KIs^*Q=SB7|1v>0-`&TiVq|97XMDzaAe#mea_ZuZV()c0^I1SLZ!3HIdd3xn(&Q(Iv*!F`t$-UEA!WB3cAi(J-HT7IqA;yik#Bs7UBUiu)kUMCPKs6joQ&INFL@YK=wc$rP(`v)5d=u^er=4q=B{1T5cqw!Vj4T5GfxHIaFyv4rfCgrdS^n-0w~#Pnocq#Ft@FG{+{G9;@Dy-!H%q%s(GuaKo}7)Iy)?MjR#Gvz-VKJ{hisY~f-G?wDgkX*7)85^DY5gSVH5ukb3gp2sV<Fip&eqY5n=N**u4g%dy-Ya0`VI=yEVh8gZl}$UGZT!vtnt{OnRuy~02CYh*3hST%ZEyyh1?R#A@J_e{u7G#L``{YrgxzpG^uZw92p@o3<^0kj{UzUQg_3I#@7wTOq0jPH(YGKcN~`oUI_q!RaC`TwpXZ%6rk&@sk8k;P-k%Z7bOdR-3i2Pn%D@K1I-e~i>L#=Y5Gl-*7VUB@f}%*%j_#-a%GUa9XD;6w-9H2$g+GEnfxF-{@LBjvxE~&ZFTr2IU&B}6tMIq*cknIv2l!9$1NbqF!He);;J?BDfPaPm4Znonz@+RZoQ|euuGAZuSDc$>s^eUJ=ibcaA{}a(I;yNqv+jekcvU&Q4N>2FWpMMlU%Sc4;VaHh>U$I3*{^<1;kznEC(g|t^&_vxC1s!3_N8e)Q@34CZ6Ryd4*$8_q&e;2YcS^l3&}_UCtY85Q(>vt7(+=81e~PMfrpL!H9Uc(PhOg=E4GzpQXBgrijiJhXnA@KG(!<ihcjU_oCg=e#c&zC3*G})LkE<g8}`5ruorHE57JAoD5f;K)0}TwN0t;ba~4g0X|wZ<VHEzdNMGylN?DmI{q?Z=3MJPf-Yb32zu|YaQ^!>RW~1WhGzz0^-lD@q(|&&SRP1&NF{e%@&UiHa_NmT;w2S3$({XxaFJ^f6#c%(Pg}3^Sl;7qZ4R7_GDsR_&ectSQg??A>1N#5reOLdt-t+T2c<0gY<lRhf*SoXc)^~aRp1q&!&Acn_?RZBOz;d<2zT0>;T&l|oA>OCU36#=jblHPOwN#grP^Y?exdKw^GrC-f&%dL~v!Ft~qRZ7-p3r5CFA^u|ath1u*X22Idg3Kro(pTe1G+pP>bxJ&8)=jD&gAWk36&Kz!hT&&V2i`L?7=qpjxHw==gqoY0gKfKb-5CsKcLIAV75A@%hg!^nJ(AB%0yC^Q&`@p%X45L(XY#M;VQ3Qm*+#H_a*dR_rehLpylsH`=5c6U>{yP@mU`Xz{&W8?&)ox-T0&z?{}hikbxa|--Ewg4o!I10_*t;m+T$t>FzCMPTE)M9O@f5IkT@cFxcJKo7s6&W=D5V>GGyb%la0qcq!7Nl|AVUdLieaACkduAHQ?72cNIUKB?ON)<Rq6!gDUpT-+CwdJ5|Y@ZsfHXE);AgFedu-j;Un?diZmgww`12!A7vt_9ZOca0qbc}UF&jIiA}P5&GBwXAJfL!(&J-2B!CGGpg6b#aaE;+h$|xCXnp#_1xWiHvBvCtWz(L(q@ZSciYa2}6j)dVIc?xnMU|85-)}xNco{?@;sluC;vwyWec9bMcoF&f5^S*vA}qF4o?If5csbc)yw94&d)QnKy;+)?k#?#q(Xl8pN;HbL&>@Z9S8o`X^59)zJI6)Pr3PlsbmG_mwi|_U+lz*E^WmJTx@Wy>ssn^5vQ<x;h3*n|r#iFJ)TSuFqW9J5(Cz?Vz7JddkPxS%!OO40AK;b&Opn{!ULz_+7^`vj$5YSQd=eS?Y9Z^g0DQBGbB_?#@#0U}<pO&YRW@cC16p*1a`Oi{AP^xdH6Sve+w&v-nLYPbF1_s#H~Kma0~>RgFriIclz|Rr8du=BqljK&90}wMZ>iOVm=eOf6R{RK03Y8H6y?TaW*rc;~TNsd5;j55tGy)9?}a3-|&&fHC}CY8A%scVM)BFMJVx4&(4SemnAr^M2$%gzK0idvMP0!zsA|d4_Z*@vpog`DWw*dPDN#a4Xybw_$Yk1U#tns!^?0C#jRwDe6?!1fNxF)LOL;KC0HMX4Rq!s#O)$X{t?aP^YVn@Ok)zIzw%Oht!$yN%)j{hdN80tv0K3)VXSlI!~RiE>K(5h3X>pPPI*4thTF5)TQb&b-B7iU8&xscBpr&cJ&_hUiCh8mAYEJUtOcFRUK-l>QsR$sa<Nf>Qde6I(5D3QF~Oc>Qnvd1~s4t)sWh&_Ng1yesz<&S$#kqP#;vcs9V)->JQZI>O*Q+eYk3GZ})oq=<61JZPwQoeJ$u~tG*WX^)!8L)7K69db+-D*4K0N^;~^DPhZcMYqRdZS@+wl`)$_!HtT+yb-&HJ-)7x!v+lQ9_uH)dZPxuZ>wcT{_?mV9&AR_Keci0_IH#rJf{s0VIx4R0Dh+j1T+zLIPltDDS2zD!*nE0LM^ArOM+M62P)Aj%f3Ul!ueV~5YYlbm_4>QIt4sSkdph<6JA0^23-(aiUm8SU)g1!^eK+=$b`4c=X>b2*z7BNn?i!LGgT5PkrL?nesH<963wl$wv~#e;U|X6`pWQbw)YZ3}lwYcwRN)|+anyx1r5pBk@9XF(^>&sjy88AGmgWqU_Vx7b?(Xd9>FpbuO_g^Kbo31M+v3ilSy!CjOzg0p3eCE(-WCd`&|(U$rm(>jPB(?qOrgybin`Eh;Ix?LMbkymKrR?KE#^s!sncTM6b-7argFic(P9gx_ZEXs!4}L<t)`1++o^%vVo+_hl?~)pgHNj+mx0`BniuT=Eu2;nxBi_yeVx}=;at+SLhgEY$#q~z-wzFTbp)jf{<kW)p0Cxrx_f#`!Op(@v!yhEGhT)Ev19k%ez^|l`=D2@J-aG9y9YXZO4Z%H`*!ZdMnhE6&j$CPQ?aX~vxE$^&)#^0dwVN)mG&SDCF!4v!G7#K+1ayqXGK@31G@;iJJ9?EHGB3BYNjg9b;`*5Vx}f1(UKYKl|p-x)V57otH%Ex{e<_+^8bIz{{bV|a<u'

font_fp = io.BytesIO(zlib.decompress(base64.b85decode(font_str)))
font = pygame.font.Font(font_fp, 16)
font_large = pygame.font.Font(font_fp, 32)

Main().run()
